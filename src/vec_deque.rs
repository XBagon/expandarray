use maybe_owned::MaybeOwned;
use std::{collections::VecDeque, fmt};

pub struct ExpandArray<T> {
    inner: VecDeque<T>,
    offset: isize,
    expand_with: fn(&Self, isize) -> T,
}

impl<T> ExpandArray<T> {
    pub fn new(expand_with: fn(&Self, isize) -> T) -> Self {
        ExpandArray {
            inner: VecDeque::new(),
            offset: 0,
            expand_with,
        }
    }

    pub fn get(&self, index: isize) -> MaybeOwned<T> {
        let len = self.inner.len() as isize;
        let front = self.offset;
        let back = len + self.offset;

        if (front..back).contains(&index) {
            (&self.inner[(index - self.offset) as usize]).into()
        } else {
            ((self.expand_with)(self, index)).into()
        }
    }

    pub fn get_mut(&mut self, index: isize) -> &mut T {
        let len = self.inner.len() as isize;
        let start = self.offset;
        let end = len + self.offset;

        if (start..end).contains(&index) {
            &mut self.inner[(index - self.offset) as usize]
        } else {
            self.set(index, (self.expand_with)(self, index));
            &mut self.inner[(index - self.offset) as usize]
        }
    }

    pub fn set(&mut self, index: isize, element: T) {
        let len = self.inner.len() as isize;
        let front = self.offset;
        let back = len + self.offset - 1;

        if !((front..=back).contains(&index)) {
            let space_to_front = front - index;
            let space_to_back = index - back;

            if space_to_back > space_to_front {
                let to_add = space_to_back.max(len);
                let first = len + self.offset;
                let mut new_values = (first..first + to_add).map(|i| (self.expand_with)(self, i)).collect::<Vec<_>>().into_iter();
                self.inner.resize_with((len + to_add) as usize, || new_values.next().unwrap());
            } else {
                let to_add = space_to_front.max(len);
                let first = self.offset - to_add;
                let mut new_values = (first..self.offset).map(|i| (self.expand_with)(self, i)).collect::<Vec<_>>().into_iter();
                self.inner.resize_with((len + to_add) as usize, || new_values.next().unwrap());
                self.offset -= to_add;
                self.inner.rotate_right(to_add as usize);
            };
        }
        self.inner[(index - self.offset) as usize] = element;
    }
}

impl<T: fmt::Debug> fmt::Debug for ExpandArray<T> {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        fmt::Debug::fmt(&self.inner, f)
    }
}

#[cfg(test)]
mod tests {
    use super::ExpandArray;

    #[test]
    fn get_only() {
        let exparr = ExpandArray::new(|ea, i| {
            if i % 4 == 0 {
                i.abs() as u32
            } else {
                (i.abs() as u32 + *ea.get(i - 1))
            }
        });
        assert_eq!(*exparr.get(0), 0);
        assert_eq!(*exparr.get(402), 1203);
        assert_eq!(*exparr.get(-402), 1209);
    }

    #[test]
    fn insert_and_get() {
        let mut exparr = ExpandArray::new(|_, i| (i * 2).abs() as u32);
        assert_eq!(*exparr.get(2), 4);
        exparr.set(1, 5);
        assert_eq!(*exparr.get(-5), 10);
        assert_eq!(*exparr.get(1), 5);
        exparr.set(-19, 66);
        assert_eq!(*exparr.get(-19), 66);
    }

    #[test]
    fn get_mut() {
        let mut exparr = ExpandArray::new(|_, i| (i * 2).abs() as u32);
        assert_eq!(*exparr.get(2), 4);
        *exparr.get_mut(5) = 1;
        assert_eq!(*exparr.get_mut(-5), 10);
        assert_eq!(*exparr.get(5), 1);
        exparr.set(-9, 12);
        assert_eq!(*exparr.get(-9), 12);
        *exparr.get_mut(-4) = 5;
        *exparr.get_mut(-9) = 230;
        assert_eq!(*exparr.get(-9), 230);
        assert_eq!(*exparr.get(-4), 5);
    }
}
