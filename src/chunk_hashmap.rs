use array_init::array_init;
use maybe_owned::MaybeOwned;
use std::{collections::HashMap, fmt};

const CHUNK_USIZE: usize = 8;
const CHUNK_ISIZE: isize = 8;

pub struct ExpandArray<T> {
    inner: HashMap<isize, [T; CHUNK_USIZE]>,
    expand_with: fn(&Self, isize) -> T,
}

impl<T> ExpandArray<T> {
    pub fn new(expand_with: fn(&Self, isize) -> T) -> Self {
        ExpandArray {
            inner: HashMap::new(),
            expand_with,
        }
    }

    pub fn get(&self, index: isize) -> MaybeOwned<T> {
        let (chunk_index, inner_index) = Self::calculate_indices(index);
        if let Some(chunk) = self.inner.get(&chunk_index) {
            (&chunk[inner_index]).into()
        } else {
            ((self.expand_with)(self, index)).into()
        }
    }

    pub fn get_mut(&mut self, index: isize) -> &mut T {
        let (chunk_index, inner_index) = Self::calculate_indices(index);
        if self.inner.contains_key(&chunk_index) {
            &mut self.inner.get_mut(&chunk_index).unwrap()[inner_index]
        } else {
            let array = array_init(|i| (self.expand_with)(self, chunk_index * CHUNK_ISIZE + i as isize));
            &mut self.inner.entry(chunk_index).or_insert(array)[inner_index]
        }
    }

    pub fn set(&mut self, index: isize, element: T) {
        self.set_get(index, element);
    }

    fn set_get(&mut self, index: isize, element: T) -> &mut T {
        let (chunk_index, inner_index) = Self::calculate_indices(index);
        if self.inner.contains_key(&chunk_index) {
            let entry = &mut self.inner.get_mut(&chunk_index).unwrap()[inner_index];
            *entry = element;
            entry
        } else {
            let mut array: [T; CHUNK_USIZE] = array_init(|i| (self.expand_with)(self, chunk_index * CHUNK_ISIZE + i as isize));
            array[inner_index] = element;
            &mut self.inner.entry(chunk_index).or_insert(array)[inner_index]
        }
    }

    fn calculate_indices(index: isize) -> (isize, usize) {
        let chunk_index = (index - CHUNK_ISIZE) / CHUNK_ISIZE;
        let inner_index = {
            let mut rest = index % CHUNK_ISIZE;
            if rest < 0 {
                rest += 8
            }
            rest as usize
        };
        (chunk_index, inner_index)
    }
}

impl<T: fmt::Debug> fmt::Debug for ExpandArray<T> {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        fmt::Debug::fmt(&self.inner, f)
    }
}

#[cfg(test)]
mod tests {
    use super::ExpandArray;

    #[test]
    fn get_only() {
        let exparr = ExpandArray::new(|ea, i| {
            if i % 4 == 0 {
                i.abs() as u32
            } else {
                (i.abs() as u32 + *ea.get(i - 1))
            }
        });
        assert_eq!(*exparr.get(0), 0);
        assert_eq!(*exparr.get(402), 1203);
        assert_eq!(*exparr.get(-402), 1209);
    }

    #[test]
    fn insert_and_get() {
        let mut exparr = ExpandArray::new(|_, i| (i * 2).abs() as u32);
        assert_eq!(*exparr.get(2), 4);
        exparr.set(1, 5);
        assert_eq!(*exparr.get(-5), 10);
        assert_eq!(*exparr.get(1), 5);
        exparr.set(-19, 66);
        assert_eq!(*exparr.get(-19), 66);
    }

    #[test]
    fn get_mut() {
        let mut exparr = ExpandArray::new(|_, i| (i * 2).abs() as u32);
        assert_eq!(*exparr.get(2), 4);
        *exparr.get_mut(5) = 1;
        assert_eq!(*exparr.get_mut(-5), 10);
        assert_eq!(*exparr.get(5), 1);
        exparr.set(-9, 12);
        assert_eq!(*exparr.get(-9), 12);
        *exparr.get_mut(-4) = 5;
        *exparr.get_mut(-9) = 230;
        assert_eq!(*exparr.get(-9), 230);
        assert_eq!(*exparr.get(-4), 5);
    }
}
